﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using ClueGame.Core;
using ClueGame.Core.Models;
using System.Collections.Generic;
using ClueGame.Models;
using System.Linq;
using System;

namespace ClueGame.Controllers
{
    public class HomeController : Controller
    {
        const string SessionKeyNewGame = "_newGame";
        const string SessionKeyPlaces = "_places";
        const string SessionKeyWeapons = "_weapons";
        const string SessionKeySuspects = "_suspects";
        const string SessionKeyCrime = "_crime";
        public IActionResult Index()
        {
            HttpContext.Session.SetInt32(SessionKeyNewGame, 1);

            return View();
        }

        [HttpGet]
        public IActionResult Game()
        {
            ViewData["Title"] = "The Game is on.";
            ViewBag.GameOver = "false";
            var newGame = HttpContext.Session.GetInt32(SessionKeyNewGame) == 1 ? true: false;
            if(newGame)
            {
                SetupGame();
            }
            var viewModel = SetViewModel(new GameViewModel());
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Game(GameViewModel viewModel) {
            var accusationResult = CheckAcusation(viewModel);
            viewModel = SetViewModel(viewModel);
            if(accusationResult == 0){
                var suspectName = viewModel.Suspects.First(x=> x.Id == viewModel.SelectedSuspectId).Name;
                var weaponName = viewModel.Weapons.First(x=> x.Id == viewModel.SelectedWeaponId).Name;
                var placeName = viewModel.Places.First(x=> x.Id == viewModel.SelectedPlaceId).Name;
                ViewBag.Message = $"Gotcha! O assassino foi o  {suspectName} com {weaponName} em {placeName}";
                ViewBag.GameOver="true";
            }else{
             ViewBag.Message = GetWrongAcusationMessage(accusationResult);
            }

            return View(viewModel);
        }
        private int CheckAcusation(GameViewModel viewModel) {
            var wrongGuess = new List<int>();
            var crime = HttpContext.Session.GetObjectFromJson<Crime>(SessionKeyCrime);
            if(viewModel.SelectedSuspectId != crime.SuspectId) {
                wrongGuess.Add(1);
            }
            if (viewModel.SelectedPlaceId != crime.PlaceId)
            {
                wrongGuess.Add(2);
            }
            if (viewModel.SelectedWeaponId != crime.WeaponId)
            {
                wrongGuess.Add(3);
            }

            if(wrongGuess.Count ==0 ) {
                return 0;
            }
            if(wrongGuess.Count ==1) {
                return wrongGuess[0];
            }
            return GetRandomTip(wrongGuess.ToArray());
        }
        private int GetRandomTip(int[] guesses) {
            var rnd = new Random();
            var idx = rnd.Next(0,guesses.Length);
            return guesses[idx];
        }
        private string GetWrongAcusationMessage(int acusationResult) {
            switch (acusationResult)
            {
                case 1:
                return "1 - Assassino incorreto";
                case 2:
                return "2 - Local incorreto";
                default:
                return "3 - Arma incorreta";
            }
        }
        private void SetupGame()
        {
            var gameLogic = new GameLogic();
            var places = gameLogic.GetPlaces();
            var weapons = gameLogic.GetWeapons();
            var suspects = gameLogic.GetSuspects();

            var crime = gameLogic.StartGame(places, suspects, weapons);

            HttpContext.Session.SetObjectAsJson(SessionKeyPlaces, places);
            HttpContext.Session.SetObjectAsJson(SessionKeyWeapons, weapons);
            HttpContext.Session.SetObjectAsJson(SessionKeySuspects, suspects);
            HttpContext.Session.SetObjectAsJson(SessionKeyCrime, crime);

            HttpContext.Session.SetInt32(SessionKeyNewGame, 0);
        }
        private GameViewModel SetViewModel(GameViewModel viewModel)
        {
            viewModel.Places = HttpContext.Session.GetObjectFromJson<IList<Place>>(SessionKeyPlaces);
            viewModel.Suspects = HttpContext.Session.GetObjectFromJson<IList<Suspect>>(SessionKeySuspects);
            viewModel.Weapons = HttpContext.Session.GetObjectFromJson<IList<Weapon>>(SessionKeyWeapons);

            return viewModel;
        }
    }
}
