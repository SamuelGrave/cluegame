using System.Collections.Generic;
using ClueGame.Core.Models;

namespace ClueGame.Models
{
    public class GameViewModel
    {
        public GameViewModel()
        {
            Weapons = new List<Weapon>();
            Places = new List<Place>();
            Suspects = new List<Suspect>();
        }
        public IList<Weapon> Weapons { get; set; }
        public IList<Place> Places { get; set; }
        public IList<Suspect> Suspects { get; set; }

        public int SelectedSuspectId { get; set; }
        public int SelectedPlaceId { get; set; }
        public int SelectedWeaponId { get; set; }
    }
}