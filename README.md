# ClueGame

## Modelo de arquitetura
Ver arquivo arquitetura.png no root do repositório.

## Sugestões para técnologias

### DataBase

### Backend
Utilizaria uma das seguintes opções para a criação de uma API Restful para o jogo:

*.Net Core
*Node JS + Express

Ambas as sugestões permitem que a equipe de desenvolvimento possa trabalhar com qualquer sistema operacional, além de permitir uma hospedagem em servidores UNIX, permitindo assim economia nos custos da mesma.

Tanto o Javascript quanto o C#, poderiam ser usados em todo o stack da aplicação. Então, minha escolha seria definida por isso. Se por exemplo, a aplicação Mobile fosse construida com Xamarin, eu utilizaria o .Net Core no backend. Se a aplicação fosse construida com React Native, já utilizaria o Node.

### Frontend
Mesmo sendo uma aplicação relativamente simples, utilizaria React + Redux para o desenvolvimento do Front. React é uma das libraries de frontend mais [utilizadas no mercado](https://www.similartech.com/compare/angular-js-vs-react-js). Há grande engajamento da comunidade e plugins compativeis.

### Mobile
Assim como backend, temos duas opções multiplataformas que eu recomendaria:

*Xamarin
*React Native

Não vejo, para esta aplicação, necessidade de utilizar as linguagens nativas do Android e do iOS para o desenvolvimento.