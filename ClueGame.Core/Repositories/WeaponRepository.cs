﻿using ClueGame.Core.Models;
using ClueGame.Core.Providers;
using System.Collections.Generic;

namespace ClueGame.Core.Repositories
{
    public class WeaponRepository : IRepository<Weapon>
    {
        private readonly IList<Weapon> _weapons;
        private string _path = "../weapons.json";
        public WeaponRepository(IProvider provider)
        {
            _weapons = provider.GetData<Weapon>(_path);
        }
        public IList<Weapon> RetrieveAll()
        {
            return _weapons;
        }
    }
}
