﻿using System.Collections.Generic;

namespace ClueGame.Core.Repositories
{
    public interface IRepository<T>
    {
        IList<T> RetrieveAll();
    }
}
