﻿using ClueGame.Core.Models;
using ClueGame.Core.Providers;
using System.Collections.Generic;

namespace ClueGame.Core.Repositories
{
    public class SuspectRepository : IRepository<Suspect>
    {
        private readonly IList<Suspect> _suspects;
        private string _path = "../suspects.json";
        public SuspectRepository(IProvider provider)
        {
            _suspects = provider.GetData<Suspect>(_path);
        }
        public IList<Suspect> RetrieveAll()
        {
            return _suspects;
        }
    }
}
