﻿using ClueGame.Core.Models;
using ClueGame.Core.Providers;
using System.Collections.Generic;

namespace ClueGame.Core.Repositories
{
    public class PlaceRepository : IRepository<Place>
    {
        private readonly IList<Place> _places;
        private string _path = "../places.json";
        public PlaceRepository(IProvider provider)
        {
            _places = provider.GetData<Place>(_path);
        }
        public IList<Place> RetrieveAll()
        {
            return _places;
        }
    }
}
