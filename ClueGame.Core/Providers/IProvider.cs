﻿using System;
using System.Collections.Generic;

namespace ClueGame.Core.Providers
{
    public interface IProvider
    {
        IList<T> GetData<T>(string path);
    }
}
