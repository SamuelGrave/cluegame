﻿namespace ClueGame.Core.Models
{
    public class Suspect
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}