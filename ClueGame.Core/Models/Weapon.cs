﻿namespace ClueGame.Core.Models
{
    public class Weapon
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}