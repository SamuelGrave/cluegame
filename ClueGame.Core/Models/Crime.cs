﻿namespace ClueGame.Core.Models
{
    public class Crime
    {
        public int PlaceId { get; set; }
        public int SuspectId { get; set; }
        public int WeaponId { get; set; }
    }
}