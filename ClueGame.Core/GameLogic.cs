﻿using ClueGame.Core.Models;
using ClueGame.Core.Providers;
using ClueGame.Core.Repositories;
using System;
using System.Collections.Generic;

namespace ClueGame.Core
{
    public class GameLogic
    {
        public Crime StartGame(IList<Place> places, IList<Suspect> suspects, IList<Weapon> weapons)
        {
            var crime = new Crime();
            var rnd = new Random();
            crime.PlaceId = rnd.Next(1, places.Count);
            crime.SuspectId= rnd.Next(1, suspects.Count);
            crime.WeaponId = rnd.Next(1, weapons.Count);

            return crime;
        }

        public IList<Place> GetPlaces()
        {
            var repository = new PlaceRepository(new JsonProvider());

            return repository.RetrieveAll();
        }
        public IList<Weapon> GetWeapons()
        {
            var repository = new WeaponRepository(new JsonProvider());

            return repository.RetrieveAll();
        }
        public IList<Suspect> GetSuspects()
        {
            var repository = new SuspectRepository(new JsonProvider());

            return repository.RetrieveAll();
        }
    }
}
